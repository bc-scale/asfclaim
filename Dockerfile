FROM node:17-alpine

RUN mkdir /app
COPY ./app/ /app/
RUN mkdir /app/storage
RUN chown -R node:node /app

ENV ASF_HOST=localhost
ENV ASF_PORT=1242
ENV ASF_PASS=secret

USER node
WORKDIR /app
ENTRYPOINT ["sh", "./run.sh"]
