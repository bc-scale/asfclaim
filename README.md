# ASFclaim
GitLab: [https://gitlab.com/docker_repos/asfclaim](https://gitlab.com/docker_repos/asfclaim)  
DockerHub: [https://hub.docker.com/r/mega349/asfclaim](https://hub.docker.com/r/mega349/asfclaim)  
Fork from: [C4illin/ASFclaim](https://github.com/C4illin/ASFclaim/) [SHA:[d1e6e2ce2aa62b147aa4878063d0076091fcb258](https://github.com/C4illin/ASFclaim/tree/d1e6e2ce2aa62b147aa4878063d0076091fcb258)]
## Automatically claims new free packages on [Steam](https://store.steampowered.com/) if available
### Needs [ArchiSteamFarm](https://github.com/JustArchiNET/ArchiSteamFarm) with IPC enabled

### Additional features:
- [x] Docker Container
- [x] Settings for IPC Host

### compose.yml
```yaml
version: '3.8'

services:
    asfclaim:
        image: mega349/asfclaim:latest
        environment:
            - ASF_HOST=localhost    # ASF-IPC Hostname or IP
            - ASF_PORT=1242         # ASF-IPC Port
            - ASF_PASS=secret       # ASF-IPC Password (Plaintext)
```
### Planed:
- [ ] Per user progress
- [ ] Package queue
- [ ] Use more than 10 packages, up to 50 per hour (steam limit)
- [ ] Changeable package source
- [ ] Filter to ignore (Demos, Videos, ...)
